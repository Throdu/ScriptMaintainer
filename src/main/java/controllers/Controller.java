/*
Copyright (c) <2016> <Steven Berg>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package controllers;

import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.ArrayList;

import models.*;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Controller implements javafx.fxml.Initializable {

    private ControllerDBAccess dba;
    private ControllerHelper helper;
    private ConsoleMessaging cm;

    @FXML
    private MenuButton lists;
    @FXML
    private MenuItem stable;
    @FXML
    private MenuItem jessieBackports;
    @FXML
    private MenuItem jessieUpdates;
    @FXML
    private MenuItem closeWindow;
    @FXML
    private MenuItem optionSettings;
    @FXML
    private MenuItem viewItems;

    @FXML
    private ListView<String> packageList;

    @FXML
    private TextArea descriptionBox;
    @FXML
    private TextArea console;
    @FXML
    private TextField searchBox;

    @FXML
    private Button addPKG;
    @FXML
    private Button removePKG;
    @FXML
    private Button customScript;
    @FXML
    private Button writeScriptButton;


    public void populateJessie() {
        packageList.setDisable(false);
        packageList.setItems(getList("stable"));
        lists.setText("Stable");
        enableSearch();
    }

    public void populateJessieBackports() {
        packageList.setDisable(false);
        packageList.setItems(getList("jessiebackports"));
        lists.setText("JessieBackports");
        enableSearch();
    }

    public void populateJessieUpdates() {
        packageList.setDisable(false);
        packageList.setItems(getList("jessieupdates"));
        lists.setText("JessieUpdates");
        enableSearch();
    }

    public void displayDescription() {
        String selected = packageList.getSelectionModel().getSelectedItem();
        updateDisplayBox(selected, lists.getText().toLowerCase());
    }

    public void addPkg() {
        String selected = packageList.getSelectionModel().getSelectedItem();
        cm.sendMessage("Adding " + selected + " to script for installation.");
        dba.addPkg(selected);
        console.setText(cm.getOutput());
    }

    public void removePkg() {
        String selected = packageList.getSelectionModel().getSelectedItem();
        dba.removePkg(selected);
        cm.sendMessage(selected + " removed from script.");
        console.setText(cm.getOutput());
    }

    public void search() {
        ObservableList<String> list = packageList.getItems();
        int result = Collections.binarySearch(list, searchBox.getText());
        if (result > 0) {
            packageList.scrollTo(result);
            packageList.getSelectionModel().select(result);
            displayDescription();
        }
    }

    public void exit() {
        Stage stage = (Stage) console.getScene().getWindow();
        closeDBA();
        stage.close();
    }

    public void writeScript() {
        ScriptWriter writer = new ScriptWriter(cm);
        cm.sendMessage("Writing script to file.");
        console.setText(cm.getOutput());

        writer.addToScript("apt-get update && apt-get upgrade");
        writer.addToScript("apt-get install " + dba.getPackagesToInstall());
        writer.addListToScript(dba.getCustomScripts());

        writer.print();

        cm.sendMessage("Script Written.");
        console.setText(cm.getOutput());
    }

    public void showHelpMenu() {
        String message = helpMenuString();
        Alert alert = new Alert(AlertType.INFORMATION, message, ButtonType.OK);
        alert.showAndWait();
    }

    private void enableSearch() {
        searchBox.setDisable(false);
    }

    private ObservableList<String> getList(String repo) {
        return helper.convertToObservable(dba.getPackageNames(repo));
    }

    private void updateDisplayBox(String name, String table) {
        descriptionBox.setText(dba.getPackageInfo(table, name));
    }

    private void setVisibility(ArrayList<String> repos) {
        helper.setRepoVisibilty(repos, stable);
        helper.setRepoVisibilty(repos, jessieBackports);
        helper.setRepoVisibilty(repos, jessieUpdates);
    }

    private void disableAllLists() {
        stable.setVisible(false);
        jessieUpdates.setVisible(false);
        jessieBackports.setVisible(false);
    }

    public void launchSettings() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/OptionsMenu.fxml"));
        helper.launchNewWindow(fxmlLoader, 200, 150, "Options");
        ((OptionsController) fxmlLoader.getController()).exit();
        disableAllLists();
        setVisibility(dba.getSettings());
        console.setText(cm.getOutput());
    }

    public void addCustomScript() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/CustomScript.fxml"));
        helper.launchNewWindow(fxmlLoader, 800, 600, "Custom Script Editor");
        ((ScriptEditorController) fxmlLoader.getController()).exit();
        console.setText(cm.getOutput());
    }

    public void showSelectedPackages() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SelectedItems.fxml"));
        helper.launchNewWindow(fxmlLoader, 300, 400, "Selected Items");
        ((SelectedItemsController) fxmlLoader.getController()).exit();
    }

    private void closeDBA() {
        dba.close();

    }

    private String helpMenuString() {
        String lineSep = System.lineSeparator() + System.lineSeparator();
        String ret = "";
        ret += "To add a package to be installed: Select the repository from Lists that contains the package, select the desired package from the list and press the \"Add To Script\" button." + lineSep;
        ret += "To remove a package currently marked for installation: Select the repository from Lists that contains the package, select the package from the list and press the \"Remove From Script\" button." + lineSep;
        ret += "To add or edit a custom script press the \"Custom Scripting\" button. From there you can create a new script, edit an existing script, and delete a script" + lineSep;
        ret += "To display all packages selected for installation open the \"Edit\" menu, and click \"View Selected Items\"" + lineSep;
        ret += "To write the script to a file press the \"Write Script\" button and choose the output destination.";
        return ret;
    }

    public void initialize(URL location, ResourceBundle resources) {
        cm = new ConsoleMessaging();
        dba = new ControllerDBAccess();
        helper = new ControllerHelper();
        searchBox.setDisable(true);
        disableAllLists();
        setVisibility(dba.getSettings());
    }
}