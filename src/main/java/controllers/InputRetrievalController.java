/*
Copyright (c) <2016> <Steven Berg>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package controllers;

import java.net.URL;
import java.util.ResourceBundle;


import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.InputRetrievalControllerDBAccess;

public class InputRetrievalController implements javafx.fxml.Initializable{
	
	private InputRetrievalControllerDBAccess dba;
	
	@FXML private Button saveButton;
	@FXML private Button cancelButton;
	@FXML private TextField textField;
	

	public void initialize(URL arg0, ResourceBundle arg1) {
		dba = new InputRetrievalControllerDBAccess();
	}
	
	public void saveName(){
		String title = textField.getText();
		
		if(title.length() < 1){
			launchErrorAlert("Identifier field cannot be empty!");
		}else{
			dba.saveCustomScript(title, "Input Script Here");
			exit();
		}
	}
	
	public void cancelWindow(){
		exit();
	}
	
	public void exit(){
		Stage stage = (Stage) saveButton.getScene().getWindow();
		stage.close();
	}
	
	private void launchErrorAlert(String message){
		Alert alert = new Alert(AlertType.ERROR, message, ButtonType.CLOSE);
		alert.showAndWait();
	}
}
