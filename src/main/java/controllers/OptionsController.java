/*
Copyright (c) <2016> <Steven Berg>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import models.OptionsControllerDBAccess;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;

public class OptionsController implements javafx.fxml.Initializable {

	private OptionsControllerDBAccess dba;
	private boolean applied;
	private boolean changed;

	@FXML private CheckBox stable;
	@FXML private CheckBox jessieBackports;
	@FXML private CheckBox jessieUpdates;

	@FXML private Button apply;
	@FXML private Button close;
	@FXML private Button save;

	public void initialize(URL location, ResourceBundle resources) {
		dba = new OptionsControllerDBAccess();
		applied = false;
		changed = false;

		selectCurrentSettings(dba.getSettings());
	}

	public void apply() {
		disableToggle();

        boolean stableSet = stable.isSelected();
        boolean backports = jessieBackports.isSelected();
        boolean updates = jessieUpdates.isSelected();

		if (changed) {
			dba.updateSetting("stable", stable.isSelected());
			dba.updateSetting("jessieBackports", jessieBackports.isSelected());
			dba.updateSetting("jessieUpdates", jessieUpdates.isSelected());
			applied = true;
		}

		disableToggle();
	}

	public void exit() {
		Stage stage = (Stage) close.getScene().getWindow();

		if (changed && !applied) {
			if (confirmationWindow() == ButtonType.YES)
				apply();
		}
		stage.close();
	}

	public void altered() {
		changed = true;
	}

	private ButtonType confirmationWindow() {
		Alert alert = new Alert(AlertType.CONFIRMATION,
				"Changes have not been applied. Do you wish to apply changes now?", ButtonType.YES, ButtonType.NO);
		alert.showAndWait();
		return alert.getResult();
	}

	private void selectCurrentSettings(ArrayList<String> repos) {
		for (String repo : repos) {
			switch (repo) {
			case "stable":
				stable.setSelected(true);
				break;

			case "jessieUpdates":
				jessieUpdates.setSelected(true);
				break;

			case "jessieBackports":
				jessieBackports.setSelected(true);
				break;
				
			}
		}
	}

	private void disableToggle() {
		apply.setDisable(!apply.isDisable());
		close.setDisable(!close.isDisable());
		stable.setDisable(!stable.isDisable());
		jessieUpdates.setDisable(!jessieUpdates.isDisable());
		jessieBackports.setDisable(!jessieBackports.isDisable());
	}
}
