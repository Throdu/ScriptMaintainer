/*
Copyright (c) <2016> <Steven Berg>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import models.ControllerHelper;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import models.ScriptEditorControllerDBAccess;

public class ScriptEditorController implements Initializable {

    private ScriptEditorControllerDBAccess dba;
    private ControllerHelper helper;

    @FXML
    private ComboBox<String> title;
    @FXML
    private Button helpMenu;
    @FXML
    private TextArea editor;
    @FXML
    private Button deleteButton;

    public void initialize(URL arg0, ResourceBundle arg1) {
        dba = new ScriptEditorControllerDBAccess();
        helper = new ControllerHelper();
        displayTitles();
        loadScript();
    }

    public void saveScript() {
        dba.updateCustomScript(title.getValue(), helper.replaceSingleQuotes(editor.getText()));
    }

    public void setNewScript() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/RetrieveInput.fxml"));
        helper.launchNewWindow(fxmlLoader, 300, 100, "Name Script");
        ((InputRetrievalController) fxmlLoader.getController()).exit();

        title.setValue(dba.getLastEntry("ID"));
        loadScript();
        title.getItems().add(title.getValue());
    }

    public void displayHelp() {
        String message = "Using the Script Identifier field you can edit a selected script" +
                " or input the identifier for the new script." + System.lineSeparator() + System.lineSeparator() +
                "To create a new script press the \"new\" button, name your script" + " and begin writing." +
                System.lineSeparator() + System.lineSeparator() + "Adding the !#/bin/bash is not required for this area.";
        Alert alert = new Alert(AlertType.INFORMATION, message, ButtonType.OK);
        alert.showAndWait();
    }

    public void loadScript() {
        String temp = title.getValue();
        if (temp.length() > 1)
            editor.setText(dba.getCustomScript(temp));
    }

    public void removeScript() {
        dba.removeCustomScript(title.getValue());
        editor.setText("");
        removeTitle(title.getValue());
    }

    public void exit() {
        Stage stage = (Stage) editor.getScene().getWindow();
        stage.close();
    }

    private void displayTitles() {
        int firstItem = 0;
        title.setItems(helper.convertToObservable(dba.getCustomScriptTitles()));
        title.setValue(title.getItems().get(firstItem));
    }

    private void removeTitle(String t){
        title.getItems().remove(t);
    }
}
