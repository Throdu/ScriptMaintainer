/*
Copyright (c) <2016> <Steven Berg>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package models;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class ControllerDBAccess {
    private DataBaseIO dbio;
    private ControllerHelper helper;

    public ControllerDBAccess() {
        dbio = DataBaseIO.getInstance();
        helper = new ControllerHelper();
    }

    public ArrayList<String> getPackageNames(String repo) {
        String sql = "SELECT PackageName FROM " + repo;
        return dbio.getAllAttributeRecords(dbio.runSelectQuery(sql), Columns.NAME.getValue());
    }

    public String getPackageInfo(String repo, String name) {
        String sql = "SELECT * FROM " + repo + " WHERE PackageName = ?";
        ArrayList<String> record = dbio.getFullRecord(dbio.runSelectQuery(sql, new ArrayList<>(Collections.singletonList(name))));
        String ret = "Package Name: " + record.get(Columns.NAME.getValue()-1) + System.lineSeparator();
        ret += "Package Version: " + record.get(Columns.VERSION.getValue()-1) + System.lineSeparator();
        ret += "Package Description: " + record.get(Columns.DESCRIPTION.getValue()-1);
        return ret;
    }

    public void addPkg(String packageName) {
        String sql = "INSERT INTO packages (PackageName) VALUES(?)";
        dbio.runUpdateQuery(sql, new ArrayList<>(Collections.singletonList(packageName)));
    }

    public void removePkg(String packageName) {
        String sql = "DELETE FROM packages WHERE PackageName = ?";
        dbio.runUpdateQuery(sql, new ArrayList<>(Collections.singletonList(packageName)));
    }

    public String getPackagesToInstall() {
        String sql = "SELECT * FROM packages";
        return helper.arrayListToString(
                dbio.getAllAttributeRecords(
                        dbio.runSelectQuery(sql),
                        Columns.NAME.getValue()));
    }

    public ArrayList<String> getCustomScripts() {
        String sql = "SELECT * FROM customscript";
        return dbio.getAllAttributeRecords(
                dbio.runSelectQuery(sql),
                Columns.SCRIPT.getValue());
    }

    public ArrayList<String> getSettings() {
        String sql = "SELECT setting FROM settings WHERE selected is ?";
        return dbio.getAllAttributeRecords(
                dbio.runSelectQuery(sql, new ArrayList<>(Collections.singletonList("true"))),
                Columns.SETTING.getValue());
    }

    public void close() {
        try {
            if (dbio.isConnected())
                dbio.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
