/*
Copyright (c) <2016> <Steven Berg>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package models;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ScriptWriter {
	
	private ConsoleMessaging cm;
	private ArrayList<String> script;
	
	public ScriptWriter(ConsoleMessaging cm){
		this.cm = cm;
		script = new ArrayList<String>();
		script.add("#!/bin/bash");
	}
	
	public void addToScript(String s){
		script.add(s);
	}
	
	public void addListToScript(List<String> list){
		for(String s : list)
			script.add(s);
	}
	
	public void print(){
		FileChooser fc = new FileChooser();
		FileChooser.ExtensionFilter ef = new FileChooser.ExtensionFilter("Shell Script files (*.sh)", "*.sh");
		fc.getExtensionFilters().add(ef);
		File f = fc.showSaveDialog(new Stage());
		
		Path file = Paths.get(f.getAbsolutePath());
		try {
			Files.write(file, script, Charset.forName("UTF-8"));
		} catch (IOException e) {
			cm.sendMessage("Failed to write script to file: " + e.getMessage());
		}
	}
}
