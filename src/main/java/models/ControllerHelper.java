/*
Copyright (c) <2016> <Steven Berg>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package models;

import java.io.IOException;
import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ControllerHelper {
	
	public ObservableList<String> convertToObservable(ArrayList<String> list){
		ObservableList<String> items = FXCollections.observableArrayList();

		for (String s : list)
			items.add(s);
		
		return items;
	}
	
	public void setRepoVisibilty(ArrayList<String> repo, MenuItem item){
		for(String r : repo){
			if(r.equalsIgnoreCase(item.getId())){
				item.setVisible(true);
				break;
			}
		}
	}
	
	public void launchNewWindow(FXMLLoader fxmlLoader, int sceneWidth, int sceneHeight, String sceneTitle){
		try {
			Parent root = fxmlLoader.load();
			Stage stage = new Stage();
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setOpacity(1);
			stage.setTitle(sceneTitle);
			stage.setScene(new Scene(root, sceneWidth, sceneHeight));
			stage.setResizable(false);
			stage.showAndWait();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String replaceSingleQuotes(String s){
        return s.replace("'", "\"");
    }

	public String arrayListToString(ArrayList<String> list){
        String ret = "";
        for(String s : list){
            ret += s + " ";
        }

        return ret;
    }
}
