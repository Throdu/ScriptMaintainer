package models;

import java.sql.*;
import java.util.ArrayList;

public class DataBaseIO {
    private static DataBaseIO ourInstance = new DataBaseIO();
    private Connection conn;
    private ArrayList<PreparedStatement> pstmtList;

    public static DataBaseIO getInstance() {
        return ourInstance;
    }

    private DataBaseIO() {
        connectDB();
        pstmtList = new ArrayList<>();
    }

    private void connectDB() {
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:databases/Database.db");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet runSelectQuery(String sql, ArrayList<String> attributes){
        ResultSet ret = null;
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement(sql);

            for(int i = 1; i <= attributes.size(); i ++)
                stmt.setString(i, attributes.get(i-1));

            pstmtList.add(stmt);
            ret = stmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ret;
    }

    public ResultSet runSelectQuery(String sql){
        ResultSet ret = null;
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement(sql);
            pstmtList.add(stmt);
            ret = stmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ret;
    }

    public void runUpdateQuery(String sql, ArrayList<String> attributes){
        PreparedStatement stmt;
         try {
             stmt = conn.prepareStatement(sql);
             for(int i = 1; i <= attributes.size(); i++)
                 stmt.setString(i, attributes.get(i-1));

             pstmtList.add(stmt);
             stmt.executeUpdate();
         } catch (SQLException e) {
             e.printStackTrace();
         }
    }

    public void runUpdateQuery(String sql){
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement(sql);
            pstmtList.add(stmt);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getSingleAttribute(ResultSet rs, int column){
        String ret = "";
        try{
            ret = rs.getString(column);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public ArrayList<String> getAllAttributeRecords(ResultSet rs, int column){
        ArrayList<String> ret = new ArrayList<>();
        try{
            while (rs.next())
                ret.add(rs.getString(column));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ret;
    }

    public ArrayList<String> getFullRecord(ResultSet rs){
        ArrayList<String> ret = new ArrayList<>();
        int columns;
        try {
            columns = rs.getMetaData().getColumnCount();
            for(int i = 1; i <= columns; i++)
                ret.add(rs.getString(i));

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ret;
    }

    public void close() throws SQLException {
        for (PreparedStatement ps : pstmtList) {
            ps.close();
        }

        conn.close();
    }

    public boolean isConnected() throws SQLException{
        return conn.isValid(1);
    }
}
