/*
Copyright (c) <2016> <Steven Berg>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package models;


import java.util.ArrayList;

public class ScriptEditorControllerDBAccess {

    private DataBaseIO dbio;

    public ScriptEditorControllerDBAccess(){
        dbio = DataBaseIO.getInstance();
    }

    public void updateCustomScript(String name, String script){
        String sql = "UPDATE customscript SET Script ='" + script + "' WHERE ScriptName = '" + name + "'";
        dbio.runUpdateQuery(sql);
    }

    public String getLastEntry(String column){
        String sql = "SELECT * FROM customscript ORDER BY " + column + " DESC LIMIT 1";
        return dbio.getSingleAttribute(dbio.runSelectQuery(sql), Columns.NAME.getValue());
    }

    public String getCustomScript(String name){
        String sql = "SELECT * FROM customscript WHERE ScriptName = '" + name + "'";
        return dbio.getSingleAttribute(dbio.runSelectQuery(sql), Columns.SCRIPT.getValue());
    }

    public void removeCustomScript(String name){
        String sql = "DELETE FROM customscript WHERE ScriptName = '" + name + "'";
        dbio.runUpdateQuery(sql);
    }

    public ArrayList<String> getCustomScriptTitles(){
        String sql = "SELECT ScriptName FROM customscript";
        return dbio.getAllAttributeRecords(dbio.runSelectQuery(sql), Columns.NAME.getValue());
    }
}
